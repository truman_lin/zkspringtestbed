package marsbase.testbed.truman.ZKSpringTestbed.model;

public  class A10TableEntry {
	private int selectedIndex ;
	private int intValue;
	private String tableName;
	private String tableDescription;
	private A10Methods methods;
	public A10TableEntry(){
		
	}
	public A10TableEntry(A10Methods methods){
		this.methods=methods;
	}
	public int getSelectedIndex() {
		return selectedIndex;
	}
	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	public int getIntValue() {
		return intValue;
	}
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getTableDescription() {
		return tableDescription;
	}
	public void setTableDescription(String tableDescription) {
		this.tableDescription = tableDescription;
	}
	public A10Methods getMethods() {
		return methods;
	}
	public void setMethods(A10Methods methods) {
		this.methods = methods;
	}
}
