/**
 * 程式資訊摘要：
 * 類別名稱：.java
 * 程式內容說明：
 * 版本資訊：
 * 程式設計人員姓名：
 * 程式修改記錄：20xx-xx-xx
 * 版權宣告：
 */
package marsbase.testbed.truman.ZKSpringTestbed.model;

/**
 * @author user
 * 
 */
public class ActionBean extends CheckBean {
    public ActionBean(String code, String name) {
	super(code, name);
    }

    public ActionBean() {

    }

    SingleFunction action;

    /**
     * @return the actioin
     */
    public SingleFunction getAction() {
	return action;
    }

    /**
     * @param actioin
     *            the actioin to set
     */
    public void setAction(SingleFunction action) {
	this.action = action;
    }
}
