package marsbase.testbed.truman.ZKSpringTestbed.model;

public class CheckBean {
	private String code;
	private String name;
	private boolean chked;
	
	public CheckBean(){
		
	}
	public CheckBean(String code,String name){
		this.code=code;
		this.name=name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isChked() {
		return chked;
	}
	public void setChked(boolean chked) {
		this.chked = chked;
	}
}
