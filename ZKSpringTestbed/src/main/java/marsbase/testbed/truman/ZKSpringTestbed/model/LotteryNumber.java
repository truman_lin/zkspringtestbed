/**
 * 程式資訊摘要：
 * 類別名稱：.java
 * 程式內容說明：
 * 版本資訊：
 * 程式設計人員姓名：
 * 程式修改記錄：20xx-xx-xx
 * 版權宣告：
 */
package marsbase.testbed.truman.ZKSpringTestbed.model;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.List;

/**
 * @author user
 * 
 */
public class LotteryNumber implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4303304266202310362L;
    private int number;
    private int position;
    private BufferedImage img;
    private boolean selected;
    private int state;
    private int hitcount;
    private List<Integer> infoList;

    public LotteryNumber(int number, int hitcount) {
	this.number = number;
	this.hitcount = hitcount;
	img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
	img.setRGB(1, 1, 0);
	Graphics2D g2d = img.createGraphics();

	g2d.setBackground(Color.lightGray);
	g2d.setStroke(new BasicStroke(5));
	g2d.setColor(Color.GRAY);

	g2d.drawArc(10, 10, 80, 80, 0, 360);
	// g2d.setColor(Color.DARK_GRAY);
	Font font = new Font("Serif", Font.PLAIN, 68);
	g2d.setFont(font);
	int x = number > 9 ? 20 : 35;
	g2d.drawString(String.valueOf(number), x, 72);
	g2d.setColor(Color.YELLOW);
	font = new Font("Serif", Font.PLAIN, 12);
	g2d.setFont(font);
	g2d.drawString(String.valueOf(hitcount), 2, 90);
    }

    public void updateImage(int index) {
	if (index == 4 && !selected) {
	    index = 0;
	}
	setState(index);
	Color[] colors = { Color.GRAY, Color.RED, Color.GREEN, Color.BLACK, Color.YELLOW };
	Graphics2D g2d = (Graphics2D) img.getGraphics();
	g2d.clearRect(0, 0, 100, 100);
	g2d.setBackground(Color.lightGray);
	g2d.setStroke(new BasicStroke(5));
	g2d.setColor(colors[index]);
	g2d.drawArc(10, 10, 80, 80, 0, 360);
	// g2d.setColor(Color.DARK_GRAY);
	Font font = new Font("Serif", index % colors.length == 0 ? Font.PLAIN : Font.BOLD,
	        index % colors.length == 0 ? 68 : 72);
	g2d.setFont(font);
	int x = number > 9 ? 20 : 35;
	g2d.drawString(String.valueOf(number), x, 76);
	if (infoList.size() != 0) {
	    g2d.drawString(String.valueOf(infoList.size()), 6, 10);
	}
	g2d.setColor(Color.YELLOW);
	font = new Font("Serif", Font.PLAIN, 12);
	g2d.setFont(font);
	g2d.drawString(String.valueOf(hitcount), 2, 92);
    }

    public void displayInfo(int corner, int clear) {
	Color[] colors = { Color.GRAY, Color.RED, Color.GREEN, Color.BLACK, Color.YELLOW };
	Graphics2D g2d = (Graphics2D) img.getGraphics();
	g2d.setStroke(new BasicStroke(5));
	g2d.setColor(colors[4 + clear]);
	// g2d.drawArc(10, 10, 80, 80, 0, 360);
	// g2d.setColor(Color.DARK_GRAY);
	// Font font = new Font("Serif", index % colors.length == 0 ? Font.PLAIN
	// : Font.BOLD,
	// index % colors.length == 0 ? 68 : 72);
	// g2d.setFont(font);
	// int x = number > 9 ? 20 : 35;
	g2d.drawString(String.valueOf(infoList.size()), 6 + corner * 80, 10);
	// firstList.get(index).setRGB(1, 1, value);
    }

    public void clearInfo() {
	Graphics2D g2d = (Graphics2D) img.getGraphics();
	g2d.clearRect(0, 0, 8, 8);
	g2d.clearRect(86, 8, 100, 8);
    }

    /**
     * @return the number
     */
    public int getNumber() {
	return number;
    }

    /**
     * @param number
     *            the number to set
     */
    public void setNumber(int number) {
	this.number = number;
    }

    /**
     * @return the img
     */
    public BufferedImage getImg() {
	return img;
    }

    /**
     * @param img
     *            the img to set
     */
    public void setImg(BufferedImage img) {
	this.img = img;
    }

    /**
     * @return the position
     */
    public int getPosition() {
	return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(int position) {
	this.position = position;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
	return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(boolean selected) {
	this.selected = selected;
    }

    /**
     * @return the state
     */
    public int getState() {
	return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(int state) {
	this.state = state;
    }

    /**
     * @return the infoList
     */
    public List<Integer> getInfoList() {
	return infoList;
    }

    /**
     * @param infoList
     *            the infoList to set
     */
    public void setInfoList(List<Integer> infoList) {
	this.infoList = infoList;
    }

    /**
     * @return the hitcount
     */
    public int getHitcount() {
	return hitcount;
    }

    /**
     * @param hitcount
     *            the hitcount to set
     */
    public void setHitcount(int hitcount) {
	this.hitcount = hitcount;
    }
}
