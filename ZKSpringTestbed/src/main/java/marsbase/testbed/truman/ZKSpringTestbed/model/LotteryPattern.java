/**
 * 程式資訊摘要：
 * 類別名稱：.java
 * 程式內容說明：
 * 版本資訊：
 * 程式設計人員姓名：
 * 程式修改記錄：20xx-xx-xx
 * 版權宣告：
 */
package marsbase.testbed.truman.ZKSpringTestbed.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

/**
 * @author user
 * 
 */
public class LotteryPattern {
    /**
     * 0:column 1:row 2:both
     */
    private static int[] counters = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	    0 };
    private final static int COLUMN_MASK = 0x0000007f;
    // private final static int ROW_MASK= 0x00007f00;
    // private int tag;
    private int value;
    private int count;
    private List<Integer> stages;
    private int ui;
    private static BufferedImage black = new BufferedImage(20, 20, BufferedImage.TYPE_INT_RGB);
    private static BufferedImage white = new BufferedImage(20, 20, BufferedImage.TYPE_INT_RGB);
    private static BufferedImage[] patternImgs = { white, black };

    static {
	Graphics2D g2d = white.createGraphics();
	g2d.setColor(Color.LIGHT_GRAY);
	g2d.fillRect(0, 0, 20, 20);
    }

    public LotteryPattern() {
    }

    /**
     * @return the value
     */
    public int getValue() {
	return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(int value) {
	this.value = value;
	setCount();
    }

    /**
     * @return the stages
     */
    public List<Integer> getStages() {
	return stages;
    }

    /**
     * @param stages
     *            the stages to set
     */
    public void setStages(List<Integer> stages) {
	this.stages = stages;
    }

    public String getPattern() {
	return decode(value);
    }

    public String getRowPattern() {
	return decode(getRowPatternValue());
    }

    public String getColPattern() {
	return decode(getColPatternValue());
    }

    public String getPatternInfo() {
	return String.format("%04d - %s[%d][%d/%d]", stages.get(0), getPattern(), 14 - count, stages.size(),
	        counters[28 - count]);
    }

    public String getRowPatternInfo() {
	return String.format("%04d - %s[%d][%d/%d]", stages.get(0), getRowPattern(), 7 - count, stages.size(),
	        counters[14 - count]);
    }

    public String getColPatternInfo() {
	return String.format("%04d - %s[%d][%d/%d]", stages.get(0), getColPattern(), 7 - count, stages.size(),
	        counters[7 - count]);
    }

    public int getRowPatternValue() {
	return value & 0x00007f00;
    }

    public int getColPatternValue() {
	return value & 0x0000007f;
    }

    /**
     * @return the count
     */
    public int getCount() {
	return count;
    }

    /**
     * display stage, patterns
     */
    public int getStage() {
	return stages.get(0);
    }

    public int getC6() {
	int pvalue = value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	return (pvalue & 0x00000040) > 0 ? 1 : 0;
    }

    public int getC5() {
	int pvalue = value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	return (pvalue & 0x00000020) > 0 ? 1 : 0;
    }

    public int getC4() {
	int pvalue = value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	return (pvalue & 0x00000010) > 0 ? 1 : 0;
    }

    public int getC3() {
	int pvalue = value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	return (pvalue & 0x00000008) > 0 ? 1 : 0;
    }

    public int getC2() {
	int pvalue = value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	return (pvalue & 0x00000004) > 0 ? 1 : 0;
    }

    public int getC1() {
	int pvalue = value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	return (pvalue & 0x00000002) > 0 ? 1 : 0;
    }

    public int getC0() {
	int pvalue = value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	return (pvalue & 0x00000001) > 0 ? 1 : 0;
    }

    public int getC7() {
	return (value & 0x00004000) > 0 ? 1 : 0;
    }

    public int getC8() {
	return (value & 0x00002000) > 0 ? 1 : 0;
    }

    public int getC9() {
	return (value & 0x00001000) > 0 ? 1 : 0;
    }

    public int getC10() {
	return (value & 0x00000800) > 0 ? 1 : 0;
    }

    public int getC11() {
	return (value & 0x00000400) > 0 ? 1 : 0;
    }

    public int getC12() {
	return (value & 0x00000200) > 0 ? 1 : 0;
    }

    public int getC13() {
	return (value & 0x00000100) > 0 ? 1 : 0;
    }

    public List<BufferedImage> getPatternList() {
	return Arrays.asList(patternImgs[getC0()], patternImgs[getC1()], patternImgs[getC2()], patternImgs[getC3()],
	        patternImgs[getC4()], patternImgs[getC5()], patternImgs[getC6()]);
    }

    private String decode(int pvalue) {
	StringBuilder result = new StringBuilder();

	int mask = 1;
	int counter = 0;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	while ((pvalue & COLUMN_MASK) != 0) {
	    for (int i = 0; i < 7; i++) {
		if ((pvalue & mask) == 0) {
		    result.append(" 0 ");
		} else {
		    result.append(" 1 ");
		    counter++;
		}
		mask = mask << 1;
	    }
	    pvalue >>= 8;
	    mask = 1;
	}
	this.count = counter;
	return result.toString();
    }

    private void setCount() {
	int mask = 1;
	int counter = 0;
	int pvalue = this.value;
	if ((pvalue & COLUMN_MASK) == 0) {
	    pvalue >>= 8;
	}
	while ((pvalue & COLUMN_MASK) != 0) {
	    for (int i = 0; i < 7; i++) {
		if ((pvalue & mask) != 0) {
		    counter++;
		}
		mask = mask << 1;
	    }
	    pvalue >>= 8;
	    mask = 1;
	}
	this.count = counter;
    }

    public int getTotal() {
	if ((value & COLUMN_MASK) == 0) {
	    return counters[14 - count];
	}
	return counters[7 - count];
    }

    /**
     * @return the counters
     */
    public static int[] getCounters() {
	return counters;
    }

    /**
     * @param counters
     *            the counters to set
     */
    public static void setCounters(int[] counters) {
	LotteryPattern.counters = counters;
    }

    /**
     * @return the ui
     */
    public int getUi() {
	return ui;
    }

    /**
     * @param ui
     *            the ui to set
     */
    public void setUi(int ui) {
	this.ui = ui;
    }
}
