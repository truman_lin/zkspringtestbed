/**
 * 程式資訊摘要：
 * 類別名稱：.java
 * 程式內容說明：
 * 版本資訊：
 * 程式設計人員姓名：
 * 程式修改記錄：20xx-xx-xx
 * 版權宣告：
 */
package marsbase.testbed.truman.ZKSpringTestbed.model;

import java.io.Serializable;

/**
 * @author user
 * 
 */
public class LotteryRecord implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -266351096379154350L;
    private String date;
    private int stage;
    private int[] numbers;
    /**
     * special number
     */
    private int snmumber;
    /**
     * record numbers in 49 of 64 bits
     */
    private long value;

    /**
     * @return the date
     */
    public String getDate() {
	return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(String date) {
	this.date = date;
    }

    /**
     * @return the stage
     */
    public int getStage() {
	return stage;
    }

    /**
     * @param stage
     *            the stage to set
     */
    public void setStage(int stage) {
	this.stage = stage;
    }

    /**
     * @return the numbers
     */
    public int[] getNumbers() {
	return numbers;
    }

    /**
     * @param numbers
     *            the numbers to set
     */
    public void setNumbers(int[] numbers) {
	this.numbers = numbers;
    }

    /**
     * @return the snmumber
     */
    public int getSnmumber() {
	return snmumber;
    }

    /**
     * @param snmumber
     *            the snmumber to set
     */
    public void setSnmumber(int snmumber) {
	this.snmumber = snmumber;
    }

    /**
     * @return the value
     */
    public long getValue() {
	return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(long value) {
	this.value = value;
    }
}
