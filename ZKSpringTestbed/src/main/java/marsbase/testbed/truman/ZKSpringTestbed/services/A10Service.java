package marsbase.testbed.truman.ZKSpringTestbed.services;

public interface A10Service {
    int searchTable1();

    int searchTable2();

    int searchTable3();

    int searchTable4();

    int searchTable5();

    int searchTable6();

    int searchTable7();

    int searchTable8();

    String delTable1();

    String moveTable1();

    String delTable2();

    String moveTable2();

    String delTable3();

    String moveTable3();

    String delTable4();

    String moveTable4();

    String delTable5();

    String moveTable5();

    String delTable6();

    String moveTable6();

    String delTable7();

    String moveTable7();

    String delTable8();

    String moveTable8();
}
