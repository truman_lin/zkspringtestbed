package marsbase.testbed.truman.ZKSpringTestbed.services.impl;

import marsbase.testbed.truman.ZKSpringTestbed.services.A10Service;

import org.springframework.stereotype.Service;

@Service("a10Service")
public class A10ServiceImpl implements A10Service {

    public int searchTable1() {
	return 2;
    }

    public String delTable1() {
	return "delete table 1\n";
    }

    public String moveTable1() {
	return "move table 1\n";
    }

    public String delTable2() {
	return "delete table 2\n";
    }

    public String moveTable2() {
	return "move table 2\n";
    }

    public String delTable3() {
	return "delete table 3\n";
    }

    public String moveTable3() {
	return "move table 3\n";
    }

    public String delTable4() {
	return "delete table 4\n";
    }

    public String moveTable4() {
	return "move table 4\n";
    }

    public String delTable5() {
	return "delete table 5\n";
    }

    public String moveTable5() {
	return "move table 5\n";
    }

    public String delTable6() {
	return "delete table 6\n";
    }

    public String moveTable6() {
	return "move table 6\n";
    }

    public String delTable7() {
	return "delete table 7\n";
    }

    public String moveTable7() {
	return "move table 7\n";
    }

    public String delTable8() {
	return "delete table 8\n";
    }

    public String moveTable8() {
	return "move table 8\n";
    }

    public int searchTable2() {

	return 0;
    }

    public int searchTable3() {

	return 2;
    }

    public int searchTable4() {

	return 3;
    }

    public int searchTable5() {

	return 0;
    }

    public int searchTable6() {

	return 3;
    }

    public int searchTable7() {

	return 0;
    }

    public int searchTable8() {

	return 1;
    }

}
