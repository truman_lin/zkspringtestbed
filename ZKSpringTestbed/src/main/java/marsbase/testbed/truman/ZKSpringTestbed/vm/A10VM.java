package marsbase.testbed.truman.ZKSpringTestbed.vm;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import marsbase.testbed.truman.ZKSpringTestbed.model.A10Methods;
import marsbase.testbed.truman.ZKSpringTestbed.model.A10TableEntry;
import marsbase.testbed.truman.ZKSpringTestbed.services.A10Service;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;

public class A10VM{
    List<A10TableEntry> table = new ArrayList<A10TableEntry>();
    private boolean conditionChecked;

    @WireVariable("a10Service")
    private A10Service a10Service;

    @Init
    public void initA10() {
	createTable();
	initTable();
    }

    private void initTable() {
	String[] tableNames = { "table 1", "table 2", "table 3", "table 4", "table 5", "table 6", "table 7", "table 8" };
	String[] tableDescriptions = { "table 1 description", "table 2 description", "table 3 description",
	        "table 4 description", "table 5 description", "table 6 description", "table 7 description",
	        "table 8 description" };
	int index = 0;
	for (A10TableEntry entry : table) {
	    entry.setTableDescription(tableDescriptions[index]);
	    entry.setTableName(tableNames[index]);
	    entry.setIntValue(entry.getMethods().search());
	    entry.setSelectedIndex(entry.getIntValue() > 0 ? 0 : -1);
	    index++;
	}
    }

    private void createTable() {
	A10TableEntry entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable1();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable1());
		} else {
		    builder.append(a10Service.moveTable1());
		}
	    }
	});
	table.add(entry);
	entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable2();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable2());
		} else {
		    builder.append(a10Service.moveTable2());
		}
	    }
	});
	table.add(entry);
	entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable3();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable3());
		} else {
		    builder.append(a10Service.moveTable3());
		}
	    }
	});
	table.add(entry);
	entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable4();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable4());
		} else {
		    builder.append(a10Service.moveTable4());
		}
	    }
	});
	table.add(entry);
	entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable5();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable5());
		} else {
		    builder.append(a10Service.moveTable5());
		}
	    }
	});
	table.add(entry);
	entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable6();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable6());
		} else {
		    builder.append(a10Service.moveTable6());
		}
	    }
	});
	table.add(entry);
	entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable7();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable7());
		} else {
		    builder.append(a10Service.moveTable7());
		}
	    }
	});
	table.add(entry);
	entry = new A10TableEntry(new A10Methods() {
	    public int search() {
		return a10Service.searchTable8();
	    }

	    public void exe(int selection, StringBuilder builder) {
		if (selection == 0) {
		    builder.append(a10Service.delTable8());
		} else {
		    builder.append(a10Service.moveTable8());
		}
	    }
	});
	table.add(entry);
    }

    @Command
    @NotifyChange({ "conditionChecked", "table" })
    public void changeCondition() {
	if (!conditionChecked) {
	    for (A10TableEntry e : table) {
		if (e.getIntValue() > 0) {
		    e.setSelectedIndex(0);
		}
	    }
	}
    }

    @Command
    @NotifyChange("message")
    public void doActions() {
	StringBuilder messages = new StringBuilder();
	for (A10TableEntry e : table) {
	    if (e.getIntValue() > 0) {
		messages.append("Dealing with ").append(e.getTableName()).append(": ");
		e.getMethods().exe(e.getSelectedIndex(), messages);
	    }
	}
	Map<String,Object> map=new HashMap<String,Object>();
	map.put("message",messages.toString());
	BindUtils.postGlobalCommand(null, null, "showMessage", map);
    }

    @Command
    public void downloadReport() throws FileNotFoundException, JRException, IllegalAccessException,
	    InvocationTargetException, NoSuchMethodException {
	List<A10TableEntry> rdata1 = new ArrayList<A10TableEntry>();
	List<A10TableEntry> rdata2 = new ArrayList<A10TableEntry>();
	for (A10TableEntry entry : table) {
	    if (entry.getIntValue() > 0) {
		rdata1.add(entry);
	    } else {
		rdata2.add(entry);
	    }
	}
	Map<String, Object> params = new HashMap<String, Object>();
	params.put("user", "tester1");
	JasperPrint jp1 = JasperFillManager.fillReport("jasperreport/Test1.jasper", params,
	        new JRBeanCollectionDataSource(rdata1, false));
	params.put("user", "tester 2");
	JasperPrint jp2 = JasperFillManager.fillReport("jasperreport/Test1.jasper", params,
	        new JRBeanCollectionDataSource(rdata2, false));
	for (JRPrintPage page : jp2.getPages()) {
	    jp1.addPage(page);
	}
	byte[] output = JasperExportManager.exportReportToPdf(jp1);
	Filedownload.save(output, "PDF", "outputtest.pdf");
    }

    public boolean isConditionChecked() {
	return conditionChecked;
    }

    public void setConditionChecked(boolean conditionChecked) {
	this.conditionChecked = conditionChecked;
    }

    public List<A10TableEntry> getTable() {
	return table;
    }
}
