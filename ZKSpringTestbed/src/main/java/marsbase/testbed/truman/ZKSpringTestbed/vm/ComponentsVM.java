package marsbase.testbed.truman.ZKSpringTestbed.vm;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;

import marsbase.testbed.truman.ZKSpringTestbed.model.ActionBean;
import marsbase.testbed.truman.ZKSpringTestbed.model.CheckBean;
import marsbase.testbed.truman.ZKSpringTestbed.model.SingleFunction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.sys.ValidationMessages;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Intbox;

public class ComponentsVM extends TestVM {
    /**
     * log
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ComponentsVM.class);
    private List<List<CheckBean>> listOfLists = new ArrayList<List<CheckBean>>();
    private List<CheckBean> checkList = new ArrayList<CheckBean>();
    private List<CheckBean> checkListM = new ArrayList<CheckBean>();
    private List<ActionBean> actionList;
    private ActionBean selectedActionBean;
    private int selectedGroup = 0;
    private String selections;

    private Date theDate = new Date();
    private int delta = 0;
    private String text = "Test 0";
    private String chosenName;
    private int rindex0 = 0;
    private int rindex1 = 0;
    private String ritem0 = "汽車-";
    private String ritem1 = "新增";
    @Min(1)
    private int atleastOneFlag = 0;
    private boolean atLeastOne;
    @Wire("#flag")
    private Intbox flag;
    /**
     * non-UI
     */
    private int gtotal = 0;

    @Init
    public void initComponentsVM(@BindingParam("vmsgs") ValidationMessages validationmsgs) {
	LOGGER.info("initializing ComponentsVM");
	initListoflists();

	for (String codeName : plants) {
	    String[] codeNname = codeName.split("\\-");
	    CheckBean bean = new CheckBean(codeNname[0], codeNname[1]);
	    checkList.add(bean);
	    checkListM.add(bean);
	}
	checkListM.add(new CheckBean("All", "All"));
	checkList.add(0, new CheckBean("All", "All"));
	chosenName = ritem0 + ritem1;
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
	LOGGER.info("After Compose");
	Selectors.wireComponents(view, this, false);
	ValidationMessages vmsgs = (ValidationMessages) Executions.getCurrent().getVariableResolver()
	        .resolveVariable("vmsgs"); // component id in zul
	vmsgs.addMessages(flag, null, "flag", new String[] { "Select one at least." });
    }

    @Command
    @NotifyChange({ "selections", "checkList", "checkListM", "atleastOneFlag" })
    public void checkSelection(@BindingParam("list") int lstndx, @BindingParam("index") int chkndx,
	    @BindingParam("comp") Intbox ibox) {
	List<CheckBean> list = lstndx == 0 ? checkList : checkListM;
	boolean flag = lstndx == 0 ? checkList.get(0).isChked() : checkListM.get(checkListM.size() - 1).isChked();
	if (list.get(chkndx).getName() == "All") {
	    for (CheckBean bean : list) {
		bean.setChked(flag);
	    }
	    if (flag) {
		atleastOneFlag = list.size();
	    } else {
		atleastOneFlag = 0;
	    }
	} else {
	    flag = true;
	    atleastOneFlag = 0;
	    for (int i = 1 - lstndx; i < list.size() - lstndx; i++) {
		flag = flag && list.get(i).isChked();
		if (list.get(i).isChked()) {
		    atleastOneFlag++;
		}
	    }
	    checkList.get(0).setChked(flag);
	    checkListM.get(checkListM.size() - 1).setChked(flag);
	}
	ValidationMessages vmsgs = (ValidationMessages) Executions.getCurrent().getVariableResolver()
	        .resolveVariable("vmsgs");
	if (atleastOneFlag == 0) {
	    vmsgs.addMessages(ibox, null, "flag", new String[] { "Select one at least." });
	} else {
	    vmsgs.clearMessages(ibox);
	}
	showSelections(lstndx);
    }

    @Command
    public void action(@BindingParam("index") int itemIndex) {
	StringBuilder builder = new StringBuilder(actionList.get(itemIndex).getName());
	selectedActionBean.getAction().action(builder);
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("message", builder.toString());
	BindUtils.postGlobalCommand(null, null, "showMessage", map);
    }

    @Command
    @NotifyChange("listOfLists")
    public void switchList() {
	initListoflists();
    }

    @Command
    @NotifyChange({ "listOfLists", "atLeastOne" })
    public void checkCodes(@BindingParam("rowIndex") int rindex, @BindingParam("pos") String position) {
	boolean bvalue = false;
	boolean selectAll = false;
	boolean allSelected = true;
	String checkedCodes = "";
	atLeastOne = false;
	if (rindex >= 0) { // select whole row
	    bvalue = listOfLists.get(rindex).get(0).isChked();
	    for (CheckBean bean : listOfLists.get(rindex)) {
		bean.setChked(!bvalue);
	    }
	} else { // check if select all
	    String[] pos = position.split("\\,");
	    int posx = Integer.valueOf(pos[0]);
	    int posy = Integer.valueOf(pos[1]);
	    if (posx * listOfLists.get(0).size() + posy == gtotal) {
		bvalue = listOfLists.get(posx).get(posy).isChked();
		selectAll = true;
	    }
	}
	for (List<CheckBean> list : listOfLists) {
	    for (CheckBean bean : list) {
		if (selectAll) {
		    bean.setChked(bvalue);
		}
		if (bean.isChked()) {
		    checkedCodes = checkedCodes.concat(bean.getCode()).concat(",");
		    atLeastOne = true;
		} else if (!"All".equals(bean.getName())) {
		    allSelected = false;
		}
	    }
	}
	listOfLists.get(listOfLists.size() - 1).get(listOfLists.get(listOfLists.size() - 1).size() - 1)
	        .setChked(allSelected);
	checkedCodes = checkedCodes.length() == 0 ? "none" : checkedCodes.substring(0, checkedCodes.length() - 1)
	        .replace(",All", "");
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("message", checkedCodes.toString());
	BindUtils.postGlobalCommand(null, null, "showMessage", map);
    }

    @Command
    @NotifyChange("theDate")
    public void modifyDate() {
	Calendar cal = Calendar.getInstance();
	cal.setTime(theDate);
	cal.add(Calendar.MONTH, delta);
	theDate = cal.getTime();
    }

    @Command
    @NotifyChange("text")
    public void tabTest() {
	text = text.substring(0, text.length() - 1) + (Integer.valueOf(text.substring(text.length() - 1)) + 1);
    }

    @Command
    @NotifyChange("chosenName")
    public void selectAName() {
	// String[] names = { "汽車-新增", "汽車-異動", "機車-新增", "機車-異動" };
	// chosenName = names[rindex0 * 2 + rindex1];
	chosenName = ritem0 + ritem1;
    }

    @Command
    public void validatioinTest() {

    }

    public List<CheckBean> getRowData(int index) {
	return checkListM.subList(index, index + 7 > checkListM.size() ? checkListM.size() : index + 7);
    }

    private void initListoflists() {
	int numColumns = 6;
	int numRows;
	@SuppressWarnings("unchecked")
	List<List<String>> lists = Arrays.asList(plants, animals);
	List<CheckBean> templist = new ArrayList<CheckBean>();

	for (String s : lists.get(selectedGroup)) {
	    String[] codeNname = s.split("\\-");
	    CheckBean bean = new CheckBean(codeNname[0], codeNname[1]);
	    templist.add(bean);
	}
	gtotal = templist.size();
	numRows = templist.size() % numColumns == 0 ? templist.size() / numColumns : templist.size() / numColumns + 1;
	List<CheckBean> newlist;
	listOfLists.clear();
	for (int j = 0; j < numRows; j++) {
	    newlist = new ArrayList<CheckBean>();
	    newlist.addAll(templist.subList(j * numColumns, (j + 1) * numColumns > templist.size() ? templist.size()
		    : (j + 1) * numColumns));
	    if (j == numRows - 1) {
		if (newlist.size() == numColumns) {
		    listOfLists.add(newlist);
		    newlist = new ArrayList<CheckBean>();
		}
		newlist.add(new CheckBean("All", "All"));
	    }
	    listOfLists.add(newlist);
	}
    }

    private void showSelections(int lstndx) {
	List<CheckBean> list = lstndx == 0 ? checkList : checkListM;
	selections = "";
	Iterator<CheckBean> itr = list.iterator();

	while (itr.hasNext()) {
	    CheckBean bean = itr.next();
	    if (bean.getName() == "All") {
		continue;
	    }
	    if (bean.isChked()) {
		selections += bean.getCode();
		selections += ",";
	    }
	}
	if (selections.length() > 1) {
	    selections = selections.substring(0, selections.length() - 1);
	}
    }

    /**
     * @return the actionList
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public List<ActionBean> getActionList() throws IllegalAccessException, InvocationTargetException,
	    NoSuchMethodException {
	actionList = setupActionList();
	return actionList;
    }

    public List<List<CheckBean>> getListOfLists() {
	return listOfLists;
    }

    private List<ActionBean> setupActionList() throws IllegalAccessException, InvocationTargetException,
	    NoSuchMethodException {
	final List<ActionBean> templist = new ArrayList<ActionBean>();
	for (String codeName : animals) {
	    String[] codeNname = codeName.split("\\-");
	    ActionBean abean = new ActionBean(codeNname[0], codeNname[1]);
	    abean.setAction(new SingleFunction() {
		public void action(StringBuilder builder) {
		    builder.append(" is going to do something.");
		}
	    });
	    templist.add(abean);
	}
	return templist;
    }

    public List<CheckBean> getCheckList() {
	return checkList;
    }

    /**
     * @return the checkListM
     */
    public List<CheckBean> getCheckListM() {
	return checkListM;
    }

    public String getSelections() {
	return selections;
    }

    public ActionBean getSelectedActionBean() {
	return selectedActionBean;
    }

    public void setSelectedActionBean(ActionBean selectedActionBean) {
	this.selectedActionBean = selectedActionBean;
    }

    public int getSelectedGroup() {
	return selectedGroup;
    }

    public void setSelectedGroup(int selectedGroup) {
	this.selectedGroup = selectedGroup;
    }

    /**
     * @return the theDate
     */
    public Date getTheDate() {
	return theDate;
    }

    /**
     * @param theDate
     *            the theDate to set
     */
    public void setTheDate(Date theDate) {
	this.theDate = theDate;
    }

    /**
     * @return the delta
     */
    public int getDelta() {
	return delta;
    }

    /**
     * @param delta
     *            the delta to set
     */
    public void setDelta(int delta) {
	this.delta = delta;
    }

    /**
     * @return the text
     */
    public String getText() {
	return text;
    }

    /**
     * @param text
     *            the text to set
     */
    public void setText(String text) {
	this.text = text;
    }

    /**
     * @return the chosenName
     */
    public String getChosenName() {
	return chosenName;
    }

    /**
     * @return the rindex0
     */
    public int getRindex0() {
	return rindex0;
    }

    /**
     * @param rindex0
     *            the rindex0 to set
     */
    public void setRindex0(int rindex0) {
	this.rindex0 = rindex0;
    }

    /**
     * @return the rindex1
     */
    public int getRindex1() {
	return rindex1;
    }

    /**
     * @param rindex1
     *            the rindex1 to set
     */
    public void setRindex1(int rindex1) {
	this.rindex1 = rindex1;
    }

    /**
     * @return the ritem0
     */
    public String getRitem0() {
	return ritem0;
    }

    /**
     * @param ritem0
     *            the ritem0 to set
     */
    public void setRitem0(String ritem0) {
	this.ritem0 = ritem0;
    }

    /**
     * @return the ritem1
     */
    public String getRitem1() {
	return ritem1;
    }

    /**
     * @param ritem1
     *            the ritem1 to set
     */
    public void setRitem1(String ritem1) {
	this.ritem1 = ritem1;
    }

    /**
     * @return the atleastOneFlag
     */
    public int getAtleastOneFlag() {
	return atleastOneFlag;
    }

    /**
     * @param atleastOneFlag
     *            the atleastOneFlag to set
     */
    public void setAtleastOneFlag(int atleastOneFlag) {
	this.atleastOneFlag = atleastOneFlag;
    }

    /**
     * @return the atLeastOne
     */
    public boolean isAtLeastOne() {
	return atLeastOne;
    }

    /**
     * @param atLeastOne
     *            the atLeastOne to set
     */
    public void setAtLeastOne(boolean atLeastOne) {
	this.atLeastOne = atLeastOne;
    }

    public List<String> getStringList() {
	return Arrays.asList("header 1", "header 2", "header 3", "header 4", "header 5", "header 6");
    }
}
