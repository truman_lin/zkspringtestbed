/**
 * 程式資訊摘要：
 * 類別名稱：.java
 * 程式內容說明：
 * 版本資訊：
 * 程式設計人員姓名：
 * 程式修改記錄：20xx-xx-xx
 * 版權宣告：
 */
package marsbase.testbed.truman.ZKSpringTestbed.vm;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import marsbase.testbed.truman.ZKSpringTestbed.model.CheckBean;
import marsbase.testbed.truman.ZKSpringTestbed.model.LotteryNumber;
import marsbase.testbed.truman.ZKSpringTestbed.model.LotteryPattern;
import marsbase.testbed.truman.ZKSpringTestbed.model.LotteryRecord;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.cyberneko.html.parsers.DOMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Popup;

/**
 * @author user
 * 
 */
public class NumberPlate {
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(NumberPlate.class);

    Integer[] plate = new Integer[49];
    private Integer[] indexes = new Integer[50]; // 1@46th
    private Integer[] secondPlate = new Integer[10];
    private int pageSize = 20;
    /**
     * Non-UI
     */
    private int countingTable[] = new int[50];
    private int speacialCount[] = new int[50];
    private List<LotteryRecord> records = new ArrayList<LotteryRecord>();
    private List<List<Integer>> emptyRows = new ArrayList<List<Integer>>();
    private List<List<Integer>> emptyColumns = new ArrayList<List<Integer>>();
    private Map<Integer, LotteryPattern> patternMap = new HashMap<Integer, LotteryPattern>();
    private Long previousValue = 0L;
    private List<LotteryNumber> numberStack = new ArrayList<LotteryNumber>();
    private List<LotteryPattern> patternStack = new ArrayList<LotteryPattern>();
    /**
     * UI
     */
    private int bigOrPower = 0;
    private int stage = 0;
    private int sortOrder;
    private int selectedMethod = 0;
    private int selectedEmpty = 0;
    private boolean guessing = false;
    private String srecord;
    private List<LotteryNumber> numberList = new ArrayList<LotteryNumber>();
    private List<LotteryPattern> patternList = new ArrayList<LotteryPattern>();
    private List<LotteryPattern> rowPatternList = new ArrayList<LotteryPattern>();
    private List<LotteryPattern> columnPatternList = new ArrayList<LotteryPattern>();
    private List<Integer> searchList = new ArrayList<Integer>();
    private List<Integer> matchList = new ArrayList<Integer>();
    private List<Integer> stageList;
    private List<Integer> secondList;
    private List<CheckBean> columnPattern = new ArrayList<CheckBean>();
    private List<CheckBean> rowPattern = new ArrayList<CheckBean>();

    @Wire("#stages")
    private Popup stages;
    @Wire("#pListbox")
    private Listbox pListbox;
    @Wire("#cpListbox")
    private Listbox cpListbox;
    @Wire("#rpListbox")
    private Listbox rpListbox;

    @SuppressWarnings("unchecked")
    @Init
    public void initNumberPlate() {
	int indexi = 6;
	int indexj = 3;
	for (int i = 0; i < 7; i++) {
	    columnPattern.add(new CheckBean("　 ", ""));
	}
	for (int i = 0; i < 7; i++) {
	    rowPattern.add(new CheckBean("　 ", ""));
	}
	for (int n = 1; n < 50; n++) {
	    // System.out.println("填入:" + n);
	    if (plate[indexi * 7 + indexj] == null) {
		plate[indexi * 7 + indexj] = n;
		indexes[n] = indexi * 7 + indexj;
		indexi++;
		indexj++;
		if (indexj > 6 && indexi > 6) {
		    indexi -= 2;
		    indexj = 6;
		} else {
		    if (indexi > 6) {
			indexi = 0;
		    }
		    if (indexj > 6) {
			indexj = 0;
		    }
		}
	    } else {
		n--;
		indexi -= 2;
		indexj--;
	    }
	}
	// mainList = Arrays.asList(plate);
	indexes[0] = 50;

	indexi = 2;
	indexj = 1;
	for (int n = 1; n < 10; n++) {
	    if (secondPlate[indexi * 3 + indexj] == null) {
		secondPlate[indexi * 3 + indexj] = n;
		indexi++;
		indexj++;
		if (indexi == 3 && indexj == 3) {
		    indexj = 2;
		    indexi = 1;
		} else {
		    if (indexi > 2) {
			indexi = 0;
		    }
		    if (indexj > 2) {
			indexj = 0;
		    }
		}
	    } else {
		n--;
		indexi -= 2;
		indexj--;
	    }
	}
	secondPlate[9] = 0;
	secondList = Arrays.asList(secondPlate);
	for (int i = 0; i < 7; i++) {
	    List<Integer> tempList = new ArrayList<Integer>();
	    emptyColumns.add(tempList);
	    tempList = new ArrayList<Integer>();
	    emptyRows.add(tempList);
	}
	File frecords = new File("data\\lotteries.ros");
	int updated = 0;
	if (frecords.exists()) {
	    ObjectInputStream ois;
	    try {
		ois = new ObjectInputStream(FileUtils.openInputStream(frecords));
		records.addAll((List<LotteryRecord>) ois.readObject());
		updated = records.size();
		parseHTMLTable(records.get(0).getStage());
		updated = records.size() - updated;
	    } catch (IOException e) {
		e.printStackTrace();
	    } catch (ClassNotFoundException e) {
		e.printStackTrace();
	    }
	} else {
	    parseHTMLTable(0);
	    updated = records.size();
	}
	if (updated > 0) {
	    try {
		FileOutputStream fos = new FileOutputStream("data\\lotteries.ros");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(records);
		oos.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
	countingHits();
	for (int i = 0; i < 49; i++) {
	    LotteryNumber number = new LotteryNumber(plate[i], countingTable[i]);
	    number.setPosition(i);
	    number.setInfoList(new ArrayList<Integer>());
	    numberList.add(number);
	}
	parsePatterns();
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
	// LOGGER.info("After Compose");
	Selectors.wireComponents(view, this, false);
    }

    //
    // @Command
    // @NotifyChange({ "numberList", "stage", "srecord", "answer",
    // "columnPattern", "rowPattern", "guessing" })
    // public void jumpTo(@BindingParam("stage") int stage) {
    // if (this.stage == stage) {
    // return;
    // }
    // this.stage = stage;
    // searchList.clear();
    // matchList.clear();
    // loadRecord(0);
    // }

    @Command
    @NotifyChange({ "numberList", "stage", "srecord", "answer", "columnPattern", "rowPattern", "guessing" })
    public void loadRecord(@BindingParam("step") int step, @BindingParam("param") int param) {
	searchList.clear();
	int next = 0;
	int max = records.get(0).getStage();
	switch (step) {
	    case -1:
	    case 1:
		next = stage + step;
		if (next > max) {
		    next = max;
		} else if (next <= 0) {
		    next = 1;
		}
		break;
	    case 2: // jump to stage from intbox
		next = param;
		searchList.clear();
		matchList.clear();
		break;
	    case 4: // jump to stage from matchlist
		next = matchList.get(param);
		break;
	    case -9: // initial
		next = max;
		break;
	    default:
		next = param;
	}
	if (this.stage == next && !guessing) {
	    return;
	}
	if (guessing) {
	    resetNumberList(0);
	    guessing = false;
	}
	this.stage = next;
	LotteryRecord record = records.get(max - stage);
	StringBuilder rbuilder = new StringBuilder("第 ");
	rbuilder.append(record.getStage()).append(" 期").append(record.getDate()).append(" : ");
	for (int i = 0, found = 0; i < 50 && found < 7 + (previousValue > 0 ? 7 : 0); i++) {
	    long mask = 1L << i;
	    if ((previousValue & mask) > 0) {
		numberList.get(i).updateImage(0);
		found++;
	    }
	    if ((record.getValue() & mask) > 0) {
		rbuilder.append(plate[i]).append(" ");
		found++;
	    }
	}
	LotteryPattern pattern = patternMap.get(packRecordValueToPattern(record.getValue()));
	if (pattern != null) {
	    rbuilder.append(" Column : ").append(pattern.getColPattern().substring(0, 14));
	    rbuilder.append("Row : ").append(pattern.getRowPattern().substring(0, 14));
	    if (pattern.getStages().size() > 1) {
		rbuilder.append("=>");
		for (Integer istage : pattern.getStages()) {
		    rbuilder.append(istage).append(" ");
		}
	    }
	}
	srecord = rbuilder.toString();
	int column = pattern.getColPatternValue();
	int row = pattern.getRowPatternValue() >> 8;
	int mask = 1;
	for (int i = 0; i < 7; i++) {
	    if (step != 5) { // 3:column pattern reset 5:row
		columnPattern.get(i).setChked((column & mask) > 0);
	    }
	    if (step != 3) {
		rowPattern.get(i).setChked((row & mask) > 0);
	    }
	    mask <<= 1;
	}
	// 更新數字圖案
	for (int i = 0; i < 6; i++) {
	    numberList.get(indexes[record.getNumbers()[i]]).updateImage(1);
	}
	numberList.get(indexes[record.getSnmumber()]).updateImage(2);
	previousValue = record.getValue();
    }

    // @Command
    // public void loadImage(@BindingParam("img") Image img,
    // @BindingParam("index") int index) {
    // img.setContent(newimg(indexes[index]));
    // }

    @Command
    @NotifyChange({ "answer", "matchList", "columnPattern", "rowPattern" })
    public void checkSelection(@BindingParam("index") Integer index) {
	matchList.clear();
	for (int i = 0; i < 49; i++) {
	    columnPattern.get(i % 7).setChked(columnPattern.get(i % 7).isChked() || numberList.get(i).isSelected());
	    rowPattern.get(i / 7).setChked(rowPattern.get(i / 7).isChked() || numberList.get(i).isSelected());
	}
	if (searchList.contains(index)) {
	    searchList.remove(index);
	} else {
	    searchList.add(index);
	}
	if (guessing) {
	    LotteryNumber number = numberList.get(index);
	    number.updateImage(number.isSelected() ? 4 : 0);
	    BindUtils.postNotifyChange(null, null, this, "numberList");
	}
    }

    @Command
    @NotifyChange({ "matchList", "numberList" })
    public void query() {
	long pattern = 0L;
	long mask = 1L;
	int corner = 0;
	if (searchList.size() > 1) {
	    if (searchList.get(0) % 7 == searchList.get(1) % 7) {
		corner = 1;
	    }
	}

	for (LotteryNumber number : numberStack) {
	    number.getInfoList().clear();
	}
	numberStack.clear();
	for (Integer dis : searchList) {
	    pattern = pattern | (mask << dis);
	}
	while ((pattern & 1L) != 1L) {
	    pattern = pattern >> 1;
	}
	matchList.clear();
	for (LotteryRecord record : records) {
	    mask = pattern;
	    for (int index = 0; index < 49; index++) {
		if ((record.getValue() & mask) == mask) {
		    matchList.add(record.getStage());
		    numberList.get(index).getInfoList().add(record.getStage());
		    numberStack.add(numberList.get(index));
		}
		mask = mask << 1;
	    }
	}
	for (LotteryNumber number : numberStack) {
	    number.displayInfo(corner, 0);
	}
    }

    public String getAnswer() {
	String ret = "";
	// if(guessing){
	// return ret;
	// }
	if (searchList.size() > 7) {
	    ret = "Too manay selections.";
	} else if (searchList.size() > 0) {
	    long pattern = 0L;
	    long mask = 1L;
	    for (Integer dis : searchList) {
		pattern = pattern | (mask << dis);
	    }
	    // StringBuilder rets = new StringBuilder();
	    for (LotteryRecord record : records) {
		if ((record.getValue() & pattern) == pattern) {
		    // rets.append(record.getStage()).append(" ");
		    matchList.add(record.getStage());
		}
	    }
	    if (matchList.size() > 0) {
		StringBuilder combo = new StringBuilder("(");
		for (Integer dis : searchList) {
		    combo.append(plate[dis]).append(" ");
		}
		ret = combo.toString().trim() + ") found in :";// +
		                                               // rets.toString();
	    } else {
		ret = "Pattern Not Found.";
	    }
	}
	return ret;
    }

    // @Command
    // @NotifyChange({ "numberList", "stage", "srecord", "answer",
    // "columnPattern", "rowPattern", "guessing" })
    // public void jumpToStage(@BindingParam("index") int index) {
    // int stage = matchList.get(index);
    // if (this.stage == stage) {
    // return;
    // }
    // this.stage = stage;
    // loadRecord(0);
    // }

    public int getBigOrPower() {
	return bigOrPower;
    }

    @Command
    @NotifyChange({ "patternList", "rowPatternList", "columnPatternList" })
    public void resort() {
	@SuppressWarnings("unchecked")
	List<Comparator<LotteryPattern>> comparators = Arrays.asList(new patternStageComparator(),
	        new patternCountComparator(), new patternHitComparator());
	Collections.sort(patternList, comparators.get(sortOrder));
	Collections.sort(rowPatternList, comparators.get(sortOrder));
	Collections.sort(columnPatternList, comparators.get(sortOrder));
    }

    @Command
    @NotifyChange("stageList")
    public void checkout(@BindingParam("cell") Component cell, @BindingParam("list") List<Integer> list) {
	stageList = list;
	stages.open(cell, "after_end");
    }

    @Command
    @NotifyChange({ "patternList", "columnPatternList", "rowPatternList" })
    public void searchPattern() {
	int pvalue = 0;
	int cmask = 1;
	int rmask = 256;
	for (LotteryPattern pattern : patternStack) {
	    pattern.setUi(0);
	}
	patternStack.clear();
	for (int i = 0; i < 7; i++) {
	    if (columnPattern.get(i).isChked()) {
		pvalue |= cmask;
	    }
	    if (rowPattern.get(i).isChked()) {
		pvalue |= rmask;
	    }
	    cmask <<= 1;
	    rmask <<= 1;
	}

	LotteryPattern selectedPattern = patternMap.get(pvalue & 0x0000007f);
	int index = -1;
	if (selectedPattern != null) {
	    index = columnPatternList.indexOf(selectedPattern);
	    if (index >= 0) {
		patternStack.add(selectedPattern);
		selectedPattern.setUi(1);
		cpListbox.setActivePage(index / pageSize);
	    }
	}
	selectedPattern = patternMap.get(pvalue & 0x00007f00);
	if (selectedPattern != null) {
	    index = rowPatternList.indexOf(selectedPattern);
	    if (index >= 0) {
		patternStack.add(selectedPattern);
		selectedPattern.setUi(1);
		rpListbox.setActivePage(index / pageSize);
	    }
	}
	selectedPattern = patternMap.get(pvalue);
	if (selectedPattern != null) {
	    index = patternList.indexOf(selectedPattern);
	    if (index >= 0) {
		patternStack.add(selectedPattern);
		selectedPattern.setUi(1);
		// pListbox.setSelectedIndex(index);
		pListbox.setActivePage(index / pageSize);
	    }
	}
    }

    @Command
    @NotifyChange({ "guessing", "numberList", "answer", "matchList", "searchList", "columnPattern", "rowPattern" })
    public void preparingToGuess() {
	if (guessing) {
	    resetNumberList(0);
	    loadRecord(0, stage);
	} else {
	    guessing = true;
	    switch (selectedMethod) {
		case 1:
		    // resetNumberList(3);
		    pickingNumbers(false);
		    break;
		case 2:
		    makeAGuess(1);
		    break;
		case 3:
		    makeAGuess(-1);
		    break;
		case 4:
		    makeAGuess(0);
		    break;
		default:
		    resetNumberList(3);
		    ;
	    }
	}
    }

    @Command
    @NotifyChange({ "numberList", "columnPattern", "rowPattern" })
    public void resetSelection() {
	searchList.clear();
	for (LotteryNumber number : numberList) {
	    number.setSelected(false);
	}
	for (int i = 0; i < 7; i++) {
	    columnPattern.get(i).setChked(false);
	    rowPattern.get(i).setChked(false);
	}
    }

    public void makeAGuess(int most) {
	StringBuilder guess = new StringBuilder();
	List<LotteryPattern> candidates = new ArrayList<LotteryPattern>();
	// 從column pattern list裡找最大筆數
	int[] counters = LotteryPattern.getCounters();
	int max = counters[0] * most;
	int colndx = 0;
	int rowndx = 7;
	if (most == 0) {
	    for (CheckBean bean : columnPattern) {
		if (bean.isChked()) {
		    colndx++;
		}
	    }
	    rowndx = 0;
	    for (CheckBean bean : rowPattern) {
		if (bean.isChked()) {
		    rowndx++;
		}
	    }
	} else {
	    for (int loop = 0; loop < 2; loop++) {
		for (int i = 1 + loop * 7; i < 7 + loop * 7; i++) {
		    if (counters[i] * most > max) {
			max = counters[i] * most;
			rowndx = i;
		    }
		}
		if (colndx == 0) {
		    colndx = rowndx;
		    guess.append(String.format("選擇空行數為[%d]之行", colndx));
		}
		max = counters[7] * most;
	    }
	    rowndx = 14 - rowndx;
	    guess.append(String.format("選擇空列數為[%d]之列\n", rowndx));
	    colndx = 7 - colndx;
	}
	for (LotteryPattern pattern : columnPatternList) {
	    if (pattern.getCount() == colndx) {
		candidates.add(pattern);
	    }
	}
	Random rand = new Random();
	int seed = candidates.size() == 1 ? 0 : rand.nextInt(candidates.size());
	if (candidates.size() > 1) {
	    Collections.sort(candidates, new patternHitComparator());
	}
	// 亂數選一columnPattern之pattern value
	colndx = candidates.get(seed).getColPatternValue();
	candidates.clear();
	guess.append(String.format("選擇空列數為[%d]之列\n", rowndx));
	for (LotteryPattern pattern : rowPatternList) {
	    if (pattern.getCount() == rowndx) {
		candidates.add(pattern);
	    }
	}
	seed = candidates.size() == 1 ? 0 : rand.nextInt(candidates.size());
	if (candidates.size() > 1) {
	    Collections.sort(candidates, new patternHitComparator());
	}
	rowndx = candidates.get(seed).getRowPatternValue() >> 8;
	int mask = 1;
	// setup pattern checkboxes
	for (int i = 0; i < 7; i++) {
	    columnPattern.get(i).setChked((colndx & mask) > 0);
	    rowPattern.get(i).setChked((rowndx & mask) > 0);
	    mask <<= 1;
	}
	BindUtils.postNotifyChange(null, null, this, "columnPattern");
	BindUtils.postNotifyChange(null, null, this, "rowPattern");
	pickingNumbers(true);
    }

    private void pickingNumbers(boolean auto) {
	List<LotteryNumber> candidates = new ArrayList<LotteryNumber>();
	List<LotteryNumber> nextRecord = new ArrayList<LotteryNumber>();
	Set<Integer> orders = new LinkedHashSet<Integer>();
	Random rand = new Random();

	for (int i = 0; i < 7; i++) {
	    orders.add(i);
	}
	if (!auto) {
	    for (LotteryNumber number : numberList) {
		if (number.isSelected()) {
		    nextRecord.add(number);
		    orders.remove(number.getPosition() % 7);
		}
	    }
	}
	resetNumberList(3);
	for (int i = 0, index; i < orders.size() && nextRecord.size() < 6; i++) {
	    index = (Integer) orders.toArray()[i];
	    if (columnPattern.get(index).isChked()) {
		for (int j = 0; j < 7; j++) {
		    if (rowPattern.get(j).isChked() && !nextRecord.contains(numberList.get(index + j * 7))) {
			candidates.add(numberList.get(index + j * 7));
		    }
		}
		if (candidates.size() > 0) {
		    if (candidates.size() > 1) {
			Collections.sort(candidates, new numberHitcountComparator());
			nextRecord.add(candidates.get(rand.nextInt(candidates.size())));
		    } else {
			nextRecord.add(candidates.get(0));
		    }
		}
	    }
	    candidates.clear();
	}
	orders.clear();

	while (orders.size() != 7) {
	    orders.add(rand.nextInt(7));
	}
	LOGGER.info(orders.toString());
	for (LotteryNumber number : nextRecord) {
	    orders.remove(number.getPosition() / 7);
	}
	LOGGER.info(orders.toString());

	while (orders.size() != 7) {
	    orders.add(rand.nextInt(7));
	}
	LOGGER.info(orders.toString() + " : " + nextRecord.size());
	while (nextRecord.size() < 6) {
	    for (int i = 0, index = 0; i < 7 && nextRecord.size() < 6; i++) {
		index = (Integer) orders.toArray()[i];
		if (rowPattern.get(index).isChked()) {
		    for (int j = 0; j < 7; j++) {
			if (columnPattern.get(j).isChked() && !nextRecord.contains(numberList.get(index * 7 + j))) {
			    candidates.add(numberList.get(index * 7 + j));
			}
		    }
		    if (candidates.size() > 0) {
			if (candidates.size() > 1) {
			    Collections.sort(candidates, new numberHitcountComparator());
			    nextRecord.add(candidates.get(rand.nextInt(candidates.size())));
			} else {
			    nextRecord.add(candidates.get(0));
			}
		    }
		}
		candidates.clear();
	    }
	}
	matchList.clear();
	searchList.clear();
	for (LotteryNumber number : nextRecord) {
	    searchList.add(number.getPosition());
	    if (number.getState() != 4) {
		number.setSelected(true);
		number.updateImage(4);
	    }
	}
    }

    public void setBigOrPower(int bigOrPower) {
	this.bigOrPower = bigOrPower;
    }

    public Integer[] getSecondPlate() {
	return secondPlate;
    }

    public List<Integer> getSecondList() {
	return secondList;
    }

    /**
     * @return the numberList
     */
    public List<LotteryNumber> getNumberList() {
	return numberList;
    }

    /**
     * @return the stage
     */
    public int getStage() {
	return stage;
    }

    /**
     * @param stage
     *            the stage to set
     */
    public void setStage(int stage) {
	this.stage = stage;
    }

    /**
     * @return the columnPattern
     */
    public List<CheckBean> getColumnPattern() {
	return columnPattern;
    }

    /**
     * @return the rowPattern
     */
    public List<CheckBean> getRowPattern() {
	return rowPattern;
    }

    /**
     * @return the srecord
     */
    public String getSrecord() {
	return srecord;
    }

    /**
     * @return the matchList
     */
    public List<Integer> getMatchList() {
	return matchList;
    }

    /**
     * @return the sortOrder
     */
    public int getSortOrder() {
	return sortOrder;
    }

    /**
     * @param sortOrder
     *            the sortOrder to set
     */
    public void setSortOrder(int sortOrder) {
	this.sortOrder = sortOrder;
    }

    /**
     * @return the current
     */
    public int getCurrent() {
	return records.get(0).getStage();
    }

    public List<LotteryNumber> getRowData(int second, int index) {
	// if (second == 0) {
	return numberList.subList(index, index + 7);
	// }
	// else {
	// return secondList.subList(index, index + 3 > 9 ? 9 : index + 3);
	// }
    }

    public List<LotteryNumber> getRowData(int index) {
	return numberList.subList(index, index + 7);
    }

    public String getReport() {
	StringBuilder report = new StringBuilder("Patterns Analysis: \n");
	// LotteryPattern pattern = patternList.get(max - stage);
	report.append("Empty Column Counts : \t");
	for (List<Integer> list : emptyColumns) {
	    report.append(list.size()).append(" ");
	}
	int sum = 0;
	for (LotteryPattern pattern : columnPatternList) {
	    sum += pattern.getCount();
	}
	report.append("\t Pattern count:").append(sum).append("/").append(columnPatternList.size()).append(" = ")
	        .append(sum / columnPatternList.size());
	report.append("\n Column Hit counts : \t");
	int total = 0;
	for (int i = 0; i < 7; i++) {
	    sum = 0;
	    for (int j = 0; j < 7; j++) {
		sum += countingTable[i + j * 7];
	    }
	    total += sum;
	    report.append(sum).append(" ");
	}
	report.append("/").append(total / 7);
	report.append("\nEmpty Row Counts : \t\t");
	for (List<Integer> list : emptyRows) {
	    report.append(list.size()).append(" ");
	}
	sum = 0;
	for (LotteryPattern pattern : rowPatternList) {
	    sum += pattern.getCount();
	}
	report.append("\t Pattern count:").append(sum).append("/").append(rowPatternList.size()).append(" = ")
	        .append(sum / rowPatternList.size());
	report.append("\n Row Hit counts : \t");
	total = 0;
	for (int i = 0; i < 7; i++) {
	    sum = 0;
	    for (int j = 0; j < 7; j++) {
		sum += countingTable[i * 7 + j];
	    }
	    total += sum;
	    report.append(sum).append(" ");
	}
	report.append("/").append(total / 7);
	sum = 0;
	for (LotteryPattern pattern : patternList) {
	    sum += pattern.getCount();
	}
	report.append("\nPattern count:").append(sum).append("/").append(patternList.size()).append(" = ")
	        .append(sum / patternList.size());
	return report.toString();
    }

    public String getHitCount(int row, int col) {
	int isum = 0;
	int jsum = 0;
	for (int i = 0; i < 7; i++) {
	    isum += countingTable[row + i];
	    jsum += countingTable[col + 7 * i];
	}
	// LOGGER.info("row " + row + " hits:{} column " + col + " hits:{}",
	// isum, jsum);
	return countingTable[row + col] + "/(" + isum / 7 + "," + jsum / 7 + ")";
    }

    /**
     * @return the rowPatternList
     */
    public List<LotteryPattern> getRowPatternList() {
	return rowPatternList;
    }

    /**
     * @return the columnPatternList
     */
    public List<LotteryPattern> getColumnPatternList() {
	return columnPatternList;
    }

    /**
     * @return the columnPatternList
     */
    public List<LotteryPattern> getPatternList() {
	return patternList;
    }

    /**
     * @return the stageList
     */
    public List<Integer> getStageList() {
	return stageList;
    }

    private void parseHTMLTable(int recs) {
	DOMParser parser = new DOMParser();
	Document document = null;
	String[] urls = { "http://www.pilio.idv.tw/ltobig/list.asp?indexpage=",
	        "http://www.pilio.idv.tw/lto/list.asp?indexpage=" };
	int itemNos[] = { 26, 28 };
	int pageInfo[] = { 28, 30 };
	int totalPages = 1;
	int distance = 2;
	try {
	    for (int pageNo = 1, diff = 0; pageNo <= totalPages && distance > 1; pageNo++, diff++) {
		parser.parse(urls[bigOrPower] + pageNo);
		document = parser.getDocument();
		NodeList bnodes = document.getFirstChild().getFirstChild().getNextSibling().getChildNodes();
		NodeList dnodes = bnodes.item(itemNos[bigOrPower]).getChildNodes();
		NodeList tnodes = dnodes.item(1).getFirstChild().getNextSibling().getChildNodes();
		// testNodeList(bnodes);
		if (totalPages < 10) {
		    totalPages = fixNumber(bnodes.item(pageInfo[bigOrPower]).getTextContent(), true);
		}
		NodeList trs = tnodes.item(1).getChildNodes();
		// testNodeList(bnodes);
		for (int j = 2; j < trs.getLength(); j += 2) {
		    NodeList tds = trs.item(j).getChildNodes();
		    LotteryRecord record = new LotteryRecord();
		    record.setStage(fixNumber(tds.item(1).getTextContent(), false));
		    LOGGER.info("stage: {}", record.getStage());
		    record.setDate(tds.item(3).getTextContent());
		    LOGGER.info(tds.item(5).getTextContent());
		    String[] temp = tds.item(5).getTextContent().split("\\,");
		    int[] nums = new int[6];
		    for (int x = 0, k = 0; x < temp.length; x++, k++) {
			int result = fixNumber(temp[x], false);
			if (result > 99) {
			    nums[k++] = result / 100;
			    // countingTable[indexes[nums[k++]]]++;
			    nums[k] = result % 100;
			    // countingTable[indexes[nums[k]]]++;
			} else {
			    nums[k] = result;
			    // countingTable[indexes[nums[k]]]++;
			}
		    }

		    long mask;
		    for (int i = 0; i < 6; i++) {
			mask = 1L << indexes[nums[i]];
			record.setValue(record.getValue() | mask);
		    }

		    record.setNumbers(nums);
		    record.setSnmumber(fixNumber(tds.item(7).getTextContent(), false));
		    mask = 1L << indexes[record.getSnmumber()];
		    record.setValue(record.getValue() | mask);
		    speacialCount[record.getSnmumber()]++;
		    if (recs == 0) {
			records.add(record);
		    } else {
			distance = record.getStage() - recs;
			if (distance == 0) {
			    break;
			}
			records.add(diff, record);
		    }
		}
	    }
	} catch (SAXException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    private void countingHits() {
	for (LotteryRecord record : records) {
	    long mask = 1L;
	    for (int i = 1; i <= 49; i++) {
		if ((record.getValue() & mask) > 0) {
		    countingTable[indexes[i]]++;
		}
		if (record.getSnmumber() == i) {
		    speacialCount[indexes[i]]++;
		}
		mask = mask << 1;
	    }
	}
    }

    private void parsePatterns() {
	// Integer[] temp = new Integer[49];
	LotteryPattern[] patterns = { new LotteryPattern(), new LotteryPattern(), new LotteryPattern() };

	int maskC = 0x0001, maskR = 0x0100;
	for (LotteryRecord record : records) {
	    maskC = 0x0001;
	    maskR = 0x0100;
	    // for (int i = 0; i < 49; i++) {
	    // temp[i] = 0;
	    // }
	    // for (int i = 0; i < 6; i++) {
	    // temp[indexes[record.getNumbers()[i]]] = record.getNumbers()[i];
	    // }
	    // temp[indexes[record.getSnmumber()]] = record.getSnmumber() * -1;
	    // List<Integer> mainList = Arrays.asList(temp);
	    // setupPatternLists(record.getStage(),mainList);
	    int recPattern = packRecordValueToPattern(record.getValue());
	    for (int i = 0; i < 7; i++) {
		if ((recPattern & maskC) == 0) {
		    emptyColumns.get(i).add(record.getStage());
		}
		if ((recPattern & maskR) == 0) {
		    emptyRows.get(i).add(record.getStage());
		}
		maskC <<= 1;
		maskR <<= 1;
	    }
	    patterns[1].setValue(recPattern);
	    patterns[0].setValue(patterns[1].getValue() & 0x0000007f);
	    patterns[2].setValue(patterns[1].getValue() & 0x00007f00);

	    for (int i = 0; i < patterns.length; i++) {
		LotteryPattern spattern = patterns[i];
		spattern.setStages(new ArrayList<Integer>());
		LotteryPattern temp;
		temp = patternMap.get(spattern.getValue());
		if (temp != null) {
		    temp.getStages().add(record.getStage());
		} else {
		    spattern.getStages().add(record.getStage());
		    patternMap.put(spattern.getValue(), spattern);
		    switch (i) {
			case 0:
			    columnPatternList.add(spattern);
			    break;
			case 1:
			    patternList.add(spattern);
			    break;
			case 2:
			    rowPatternList.add(spattern);
			    break;
		    }
		}
		patterns[i] = new LotteryPattern();
	    }
	}
	@SuppressWarnings("unchecked")
	List<List<LotteryPattern>> lists = Arrays.asList(columnPatternList, rowPatternList, patternList);
	int base = 7;
	LotteryPattern.setCounters(new int[28]);
	for (List<LotteryPattern> list : lists) {
	    for (LotteryPattern lpattern : list) {
		LotteryPattern.getCounters()[base - lpattern.getCount()] += lpattern.getStages().size();
	    }
	    base *= 2;
	}
	loadRecord(-9, 0);
    }

    private int fixNumber(String num, boolean forPageNo) {
	if (forPageNo) {
	    while (!num.substring(0, 1).matches("[0-9]")) {
		num = num.substring(1);
	    }
	    num = num.substring(0, 2);
	}
	byte[] what = num.getBytes();
	byte[] fixed = new byte[what.length];
	int counter = 0;
	for (byte b : what) {
	    if (b > 47 && b < 58) {
		fixed[counter++] = b;
	    }
	}
	int result = fixed[0] - 48;
	for (int i = 1; i < fixed.length && fixed[i] != 0; i++) {
	    if (i == 2 && fixed[i] == 48 && fixed.length > 3) {
		result = result * 100 + fixed[3] - 48;
		break;
	    }
	    result = result * 10 + (fixed[i] - 48);
	}
	return result;
    }

    private void resetNumberList(int index) {
	for (LotteryNumber number : numberList) {
	    number.updateImage(0);
	    number.setSelected(false);
	}
	if (index > 0) {
	    for (int i = 0; i < 7; i++) {
		if (!columnPattern.get(i).isChked()) {
		    for (int j = 0; j < 7; j++) {
			numberList.get(i + 7 * j).updateImage(3);
		    }
		}
		if (!rowPattern.get(i).isChked()) {
		    for (int j = 0; j < 7; j++) {
			numberList.get(i * 7 + j).updateImage(3);
		    }
		}
	    }
	}
    }

    private void testNodeList(NodeList nodes) {
	for (int i = 0; i < nodes.getLength(); i++) {
	    // LOGGER.info("{}--Name:{} Type:{} Value:{} TextContent:{}", i,
	    // nodes.item(i).getNodeName(), nodes.item(i)
	    // .getNodeType(), nodes.item(i).getNodeValue(),
	    // nodes.item(i).getTextContent());
	    ;
	}
    }

    private int genRandom(int seed) {
	if (seed > 1) {
	    Random rand = new Random();
	    return (rand.nextInt(seed) + rand.nextInt(seed)) % 7;
	}
	return 0;
    }

    private int packRecordValueToPattern(long value) {
	long maskR = 1L;
	long maskC = 1L;
	int imaskC = 1, imaskR = 256;
	int valR = 0, valC = 0;
	int ret = 0;
	// StringBuilder srow = new StringBuilder(), scol = new StringBuilder();

	for (int i = 0; i < 7; i++) {
	    maskC = 1L << i;
	    for (int j = 0; j < 7; j++) {
		if ((value & maskR) > 0) {
		    // LOGGER.info("i:{}, j:{}, value:{" + plate[i * 7 + j] +
		    // "}", i, j);
		    valR++;
		}
		// valR += (value & maskR);
		maskR = maskR << 1;
		if ((value & maskC) > 0) {
		    // LOGGER.info("j:{}, i:{}, value:{" + plate[i + j * 7] +
		    // "}", j, i);
		    valC++;
		}
		// valC += (value & maskC);
		maskC = maskC << 7;
	    }
	    if (valR > 0) {
		ret = ret | imaskR;
		valR = 0;
		// srow = srow.append("1 ");
	    }// else {
	     // srow = srow.append("0 ");
	     // }
	    if (valC > 0) {
		ret = ret | imaskC;
		valC = 0;
		// scol = scol.append("1 ");
	    } // else {
	      // scol = scol.append("0 ");
	      // }

	    imaskR = imaskR << 1;
	    imaskC = imaskC << 1;
	}
	// retMap.put("row", srow.toString());
	// retMap.put("col", scol.toString());
	return ret;
    }

    private BufferedImage newimg(int num) {
	BufferedImage bi = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
	bi.setRGB(1, 1, 0);
	Graphics2D g2d = bi.createGraphics();

	g2d.setBackground(Color.lightGray);
	g2d.setStroke(new BasicStroke(5));
	g2d.setColor(Color.GRAY);

	g2d.drawArc(10, 10, 80, 80, 0, 360);
	// g2d.setColor(Color.DARK_GRAY);
	Font font = new Font("Serif", Font.PLAIN, 68);
	g2d.setFont(font);
	int x = num > 9 ? 20 : 35;
	g2d.drawString(String.valueOf(num), x, 72);
	return bi;
    }

    /**
     * @return the guessing
     */
    public boolean isGuessing() {
	return guessing;
    }

    /**
     * @param guessing
     *            the guessing to set
     */
    public void setGuessing(boolean guessing) {
	this.guessing = guessing;
    }

    /**
     * @return the selectedMethod
     */
    public int getSelectedMethod() {
	return selectedMethod;
    }

    /**
     * @param selectedMethod
     *            the selectedMethod to set
     */
    public void setSelectedMethod(int selectedMethod) {
	this.selectedMethod = selectedMethod;
    }

    /**
     * @return the selectedEmpty
     */
    public int getSelectedEmpty() {
	return selectedEmpty;
    }

    /**
     * @param selectedEmpty
     *            the selectedEmpty to set
     */
    public void setSelectedEmpty(int selectedEmpty) {
	this.selectedEmpty = selectedEmpty;
    }

    class patternStageComparator implements Comparator<LotteryPattern> {
	public int compare(LotteryPattern o1, LotteryPattern o2) {
	    return new CompareToBuilder().append(o2.getStages().get(0), o1.getStages().get(0)).toComparison();
	}
    }

    class patternCountComparator implements Comparator<LotteryPattern> {
	public int compare(LotteryPattern o1, LotteryPattern o2) {
	    return new CompareToBuilder().append(o1.getCount(), o2.getCount())
		    .append(o2.getStages().size(), o1.getStages().size()).toComparison();
	}
    }

    class patternHitComparator implements Comparator<LotteryPattern> {
	public int compare(LotteryPattern o1, LotteryPattern o2) {
	    return new CompareToBuilder().append(o2.getStages().size(), o1.getStages().size())
		    .append(o1.getCount(), o2.getCount()).toComparison();
	}
    }

    class numberHitcountComparator implements Comparator<LotteryNumber> {
	public int compare(LotteryNumber o1, LotteryNumber o2) {
	    return new CompareToBuilder().append(o2.getHitcount(), o1.getHitcount()).toComparison();
	}
    }

}