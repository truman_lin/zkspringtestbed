/**
 * 程式資訊摘要：
 * 類別名稱：.java
 * 程式內容說明：
 * 版本資訊：
 * 程式設計人員姓名：
 * 程式修改記錄：20xx-xx-xx
 * 版權宣告：
 */
package marsbase.testbed.truman.ZKSpringTestbed.vm;

import java.util.Calendar;

import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

/**
 * @author user
 * 
 */
public class StudyAreaVM extends TestVM {

    @Init(superclass = true)
    public void initStudyArea() {
	test1();
    }

    @NotifyChange("message")
    private void test1() {
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.MONTH, -2);
	displayMessage(cal.getTime().toString());
    }
}
