package marsbase.testbed.truman.ZKSpringTestbed.vm;

import java.util.Arrays;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;

public class TestVM {
    protected List<String> plants = Arrays.asList("A-Apple", "B-Banana", "C-Cherry", "D-Dragon Fruit", "E-Eggfruit",
	    "F-Fig", "G-Grape", "H-Honeydew", "I-Ita Palm", "J-Jack Fruit", "K-Kiwi", "L-Lemon", "M-Melon",
	    "N-Nectarine", "O-Orange", "P-Pineapple", "Q-Quince", "R-Raspberry", "S-Strawberry");
    protected List<String> animals = Arrays.asList("A-Alligator", "B-Butterfly", "C-Cat", "D-Dog", "E-Elephant",
	    "F-Fox", "G-Giraffe", "H-Hippo", "I-Inchworm", "J-Jellyfish", "K-Koala", "L-Lion", "M-Monkey", "N-Newt",
	    "O-Owl", "P-Peacock", "Q-Qeen Bee", "R-Raccon", "S-Skunk", "T-Turtle");
    private String message;

    @GlobalCommand
    @NotifyChange("message")
    public void showMessage(@BindingParam("message") String msg) {
	message = msg;
    }

    @NotifyChange("message")
    protected void displayMessage(String msg) {
	message = msg;
    }

    public String getMessage() {
	return message;
    }

	public List<String> getPlants() {
		return plants;
	}

	public void setPlants(List<String> plants) {
		this.plants = plants;
	}

	public List<String> getAnimals() {
		return animals;
	}

	public void setAnimals(List<String> animals) {
		this.animals = animals;
	}
}
